#!/usr/bin/env ruby
#encoding: utf-8

require 'rubyhelper'
require 'pry'
require 'time'

`pacman -Q -vi > list.raw`

raw = IO.read('list.raw').split("\n")[8..-1].compacti
data = {}

id = nil
raw.each do |line|
  next if line == ""
  line = line.split(" : ").strip
  if line[0] == "Nom" or line[0] == "Name"
    id = line[1]
    data[id] ||= {}
  else
    data[id][line[0]] = line[1]
  end
end

IO.write('list.json', data.to_s)

out = {}
data.each do |name, off|
  out[name] = DateTime.parse(off['Installé le']) rescue
  out[name] ||= DateTime.new
end
out = out.to_a.sort_by{|n,d| d}
out.map{|e| [e[0], e[1].strftime('%d/%m/%Y')] }

f = File.open('list.txt', 'w')
name_max = 0
out.each {|e| name_max = e[0].size if e[0].size > name_max }
out.each do |e|
  str = "#{e[0].static(name_max)} : #{e[1]}\n"
  print str
  f << str
end
f.close

